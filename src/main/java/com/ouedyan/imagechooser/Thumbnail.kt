package com.ouedyan.imagechooser

import android.widget.ImageView

class Thumbnail(val thumbnailPath: String, val thumbnailView: ImageView) {
    var isSelected: Boolean
        get() = thumbnailView.isSelected
        set(selected) {
            thumbnailView.isSelected = selected
            thumbnailView.alpha = if (selected) 0.5f else 1.0f
        }
}