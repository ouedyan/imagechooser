package com.ouedyan.imagechooser

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.getFilePath
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.with
import com.squareup.picasso.Picasso
import java.io.File
import kotlin.collections.ArrayList
import kotlin.math.max

class ImageChooser @JvmOverloads constructor(
        context: Context,
        attributeSet: AttributeSet? = null,
        defStyleAttr: Int = 0) : LinearLayout(context, attributeSet, defStyleAttr), View.OnClickListener {
    private var mContext: Context = context
    private var mMaxPictures = 0
    private var mThumbnails: ArrayList<Thumbnail> = ArrayList()
    private var mTakePicturePath: String? = null

    //Resources
    private var mThumbnailWidth = 0f
    private var mThumbnailHeight = 0f
    private var mThumbnailsMessageString: String? = null

    /**
     * Controls
     */
    //Actions
    private var mTakePicture: Button
    private var mAttachPicture: Button
    private var mRemovePicture: Button
    private var mCancel: Button

    //Containers
    private var mThumbnailsWrapper: HorizontalScrollView
    private var mThumbnailsContainer: LinearLayout
    private var mThumbnailsCount: TextView

    //Toolbars
    private var mToolbarAddActions: LinearLayout
    private var mToolbarRemoveActions: LinearLayout
    //TODO: http://ryanharter.com/blog/2014/08/29/building-dynamic-custom-views/
    /**
     * Lifecycle methods
     * https://speakerdeck.com/cyrilmottier/deep-dive-into-android-state-restoration
     * https://github.com/CharlesHarley/Example-Android-SavingInstanceState/blob/master/src/com/example/android/savinginstancestate/views/LockCombinationPicker.java
     * https://gist.github.com/granoeste/4037468
     */
    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        val savedState = SavedState(superState)
        savedState.thumbnailsPaths = thumbnailsPaths
        savedState.takePicturePath = mTakePicturePath
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val savedState = state as SavedState
        super.onRestoreInstanceState(savedState.superState)
        val thumbnailsPaths = savedState.thumbnailsPaths
        val takePicturePath = savedState.takePicturePath
        restoreState(thumbnailsPaths, takePicturePath)
    }

    init {
        //Setup defaults
        mMaxPictures = DEFAULT_MAX_PICTURES
        mThumbnailHeight = mContext.resources.getDimension(R.dimen.patio_default_thumbnail_height)
        mThumbnailWidth = mContext.resources.getDimension(R.dimen.patio_default_thumbnail_width)
        mThumbnailsMessageString = mContext.resources.getString(R.string.patio_thumbnails_message)
        orientation = VERTICAL

        //Local defaults
        var thumbnailsContainerPadding = mContext.resources.getDimension(R.dimen.patio_default_thumbnails_container_padding)
        var toolbarBackgroundColor = ContextCompat.getColor(mContext, R.color
                .patio_default_toolbar_background_color)
        var actionsTextColor = ContextCompat.getColor(mContext, R.color.patio_default_action_text_color)
        var thumbnailsWrapperBackground = ContextCompat.getColor(mContext, R.color.patio_default_thumbnails_container_background)


        //Inflate view and setup controls
        val inflater = LayoutInflater.from(mContext)
        inflater.inflate(WIDGET_LAYOUT_RES_ID, this, true)
        //Buttons
        mTakePicture = findViewById(R.id.patio_action_take_picture)
        mAttachPicture = findViewById(R.id.patio_action_attach_picture)
        mRemovePicture = findViewById(R.id.patio_action_remove_picture)
        mCancel = findViewById(R.id.patio_action_cancel)
        //Containers
        mThumbnailsWrapper = findViewById(R.id.patio_thumbnails_wrapper)
        mThumbnailsContainer = findViewById(R.id.patio_thumbnails_container)
        mThumbnailsCount = findViewById(R.id.patio_thumbnails_count_message)
        //Toolbars
        mToolbarAddActions = findViewById(R.id.patio_add_actions_toolbar)
        mToolbarRemoveActions = findViewById(R.id.patio_remove_actions_toolbar)
        mToolbarRemoveActions.visibility = GONE

        //Set actions listeners
        if (!isInEditMode) {
            mTakePicture.setOnClickListener(this)
            mAttachPicture.setOnClickListener(this)
            mRemovePicture.setOnClickListener(this)
            mCancel.setOnClickListener(this)
        }

        //Get defined attributes
        if (attributeSet != null) {
            val a = mContext.theme.obtainStyledAttributes(attributeSet, R.styleable.Patio, 0, 0)
            try {
                //Runtime
                mMaxPictures = a.getInt(R.styleable.Patio_maxPictures, DEFAULT_MAX_PICTURES)
                mThumbnailHeight = a.getDimension(R.styleable.Patio_thumbnailHeight, mThumbnailHeight)
                mThumbnailWidth = a.getDimension(R.styleable.Patio_thumbnailWidth, mThumbnailWidth)
                mThumbnailsMessageString = a.getString(R.styleable.Patio_thumbnailsMessage)
                //Local
                thumbnailsContainerPadding = a.getDimension(R.styleable.Patio_thumbnailsContainerPadding, thumbnailsContainerPadding)
                toolbarBackgroundColor = a.getColor(R.styleable.Patio_toolbarBackgroundColor, toolbarBackgroundColor)
                actionsTextColor = a.getColor(R.styleable.Patio_actionsTextColor, actionsTextColor)
                thumbnailsWrapperBackground = a.getColor(R.styleable.Patio_thumbnailsContainerBackground, thumbnailsWrapperBackground)
            } finally {
                a.recycle()
            }
        }

        //Check Max Pictures
        if (mMaxPictures <= 0) {
            mMaxPictures = DEFAULT_MAX_PICTURES
        }

        //Check Thumbnail Message
        if (mThumbnailsMessageString == null)
            mThumbnailsMessageString = mContext.resources.getString(R.string.patio_thumbnails_message)

        //Setup actions text color
        mTakePicture.setTextColor(actionsTextColor)
        mAttachPicture.setTextColor(actionsTextColor)
        mRemovePicture.setTextColor(actionsTextColor)
        mCancel.setTextColor(actionsTextColor)

        //Setup toolbar background color
        mToolbarAddActions.setBackgroundColor(toolbarBackgroundColor)
        mToolbarRemoveActions.setBackgroundColor(toolbarBackgroundColor)

        //Setup thumbnails container background
        mThumbnailsWrapper.setBackgroundColor(thumbnailsWrapperBackground)

        //Setup dimensions
        val paddingTop: Int
        val paddingBottom: Int
        val paddingLeft: Int
        val paddingRight: Int
        //Thumbnails Wrapper height
        val layoutParams: ViewGroup.LayoutParams = mThumbnailsWrapper.layoutParams
        layoutParams.height = mThumbnailHeight.toInt()
        mThumbnailsWrapper.layoutParams = layoutParams
        //Thumbnails Container padding
        paddingRight = thumbnailsContainerPadding.toInt()
        paddingLeft = thumbnailsContainerPadding.toInt()
        paddingBottom = thumbnailsContainerPadding.toInt()
        paddingTop = thumbnailsContainerPadding.toInt()
        mThumbnailsContainer.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)

        //Init Thumbnails Message
        updateThumbnailsMessage()
    }

    private fun restoreState(thumbnailsPaths: ArrayList<String>?, takePicturePath: String?) {
        thumbnailsPaths?.let {
            for (thumbnailPath in it) {
                addThumbnail(thumbnailPath)
            }
        }
        mTakePicturePath = takePicturePath
    }

    private fun addThumbnail(thumbnailPath: String) {
        val inflater = LayoutInflater.from(mContext)
        val imageView = inflater.inflate(THUMBNAIL_LAYOUT_RES_ID, mThumbnailsContainer, false) as ImageView
        val resizeDimension = max(mThumbnailWidth, mThumbnailHeight).toInt()

        Picasso.get()
                .load(File(thumbnailPath))
                .resize(resizeDimension, resizeDimension)
                .centerCrop()
                .placeholder(R.mipmap.no_images)
                .into(imageView)
        val layoutParams = imageView.layoutParams
        layoutParams.width = mThumbnailWidth.toInt()
        layoutParams.height = mThumbnailHeight.toInt()
        mThumbnailsContainer.addView(imageView, 0, layoutParams)
        imageView.setOnClickListener(this)
        mThumbnails.add(Thumbnail(thumbnailPath, imageView))
        showOrHideThumbnails()
        updateThumbnailsMessage()
    }

    private fun updateThumbnailsMessage() {
        val count = mThumbnails.size
        val thumbnailsCountMessage = String.format(mThumbnailsMessageString!!, count, mMaxPictures)
        mThumbnailsCount.text = thumbnailsCountMessage

        //Check actions button if max pictures reached
        val actionsEnabled = mThumbnails.size < mMaxPictures
        mTakePicture.isEnabled = actionsEnabled
        mAttachPicture.isEnabled = actionsEnabled
    }

    fun handleImagePickerResult(data: Intent?) {
        val filePath = getFilePath(data)
        Log.i(TAG, "File Path: $filePath")
        if (filePath != null) {
            addThumbnail(filePath)
        }
    }

    private fun showAddToolbar() {
        if (mToolbarAddActions.visibility == VISIBLE) return
        mToolbarAddActions.visibility = VISIBLE
        mToolbarRemoveActions.visibility = GONE
    }

    private fun showRemoveToolbar() {
        if (mToolbarRemoveActions.visibility == VISIBLE) return
        mToolbarRemoveActions.visibility = VISIBLE
        mToolbarAddActions.visibility = GONE
    }

    private fun cancelThumbnailSelection() {
        for (thumbnail in mThumbnails) {
            thumbnail.isSelected = false
        }
        checkToolbarsStatus()
    }

    private fun removeSelectedThumbnails() {
        for (i in mThumbnails.indices.reversed()) {
            if (mThumbnails[i].isSelected) {
                val thumbnailView: ImageView = mThumbnails[i].thumbnailView
                mThumbnailsContainer.removeView(thumbnailView)
                mThumbnails.removeAt(i)
            }
        }
        updateThumbnailsMessage()
        showOrHideThumbnails()
        checkToolbarsStatus()
    }

    private fun showOrHideThumbnails() {
        Log.d(TAG, "Number of Thumbnails: " + mThumbnails.size)
        if (mThumbnails.isNotEmpty()) {
            mThumbnailsWrapper.visibility = VISIBLE
        } else {
            mThumbnailsWrapper.visibility = GONE
        }
    }

    private fun checkToolbarsStatus() {
        var thumbnailsSelected = false
        for (thumbnail in mThumbnails) {
            if (thumbnail.thumbnailView.alpha == 0.5f) {
                thumbnailsSelected = true
                break
            }
        }
        if (thumbnailsSelected) showRemoveToolbar() else showAddToolbar()
    }

    private val thumbnailsPaths: ArrayList<String>
        get() {
            val thumbnailsPaths = ArrayList<String>()
            for (thumbnail in mThumbnails) {
                thumbnailsPaths.add(thumbnail.thumbnailPath)
            }
            return thumbnailsPaths
        }

    /**
     * OnClick buttons methods
     */
    override fun onClick(view: View) {
        //Buttons
        if (view is Button) {
            //TODO Try to get Activity result of taking pictures in library itself(think will be difficult)
            if (view.getId() == R.id.patio_action_take_picture) {
                with(mContext as Activity)
                        .cameraOnly()
                        .compress(1024)
                        .start()
            }
            if (view.getId() == R.id.patio_action_attach_picture) {
                with(mContext as Activity)
                        .galleryOnly()
                        .compress(1024)
                        .start()
            }
            if (view.getId() == R.id.patio_action_remove_picture) {
                removeSelectedThumbnails()
            }
            if (view.getId() == R.id.patio_action_cancel) {
                cancelThumbnailSelection()
            }
        }

        //Thumbnails
        if (view is ImageView) {
            //Check for Thumbnail
            for (thumbnail in mThumbnails) {
                //Inverse selection for selected thumbnail
                if (thumbnail.thumbnailView == view) {
                    thumbnail.isSelected = (!thumbnail.isSelected)
                    break
                }
            }
            checkToolbarsStatus()
        }
    }

    /**
     * SavedState class for restoring view state
     */
    private class SavedState : BaseSavedState {
        var thumbnailsPaths: ArrayList<String>? = null
        var takePicturePath: String? = null

        constructor(superState: Parcelable?) : super(superState) {}
        constructor(parcelIn: Parcel) : super(parcelIn) {
            thumbnailsPaths = parcelIn.readArrayList(String::class.java.classLoader) as ArrayList<String>
            takePicturePath = parcelIn.readString()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeList(thumbnailsPaths as List<*>?)
            out.writeString(takePicturePath)
        }

        companion object {
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(parcelIn: Parcel): SavedState {
                    return SavedState(parcelIn)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    companion object {
        val TAG = ImageChooser::class.java.simpleName
        const val DEFAULT_MAX_PICTURES = 3

        /**
         * ImageChooser ImagePicker library default request code
         */
        const val REQUEST_CODE = 2404

        /**
         * Variables
         */
        val WIDGET_LAYOUT_RES_ID = R.layout.patio_layout
        val THUMBNAIL_LAYOUT_RES_ID = R.layout.patio_thumbnail
    }
}